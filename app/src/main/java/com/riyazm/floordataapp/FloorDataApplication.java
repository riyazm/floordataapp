package com.riyazm.floordataapp;

import android.app.Application;

import com.kinvey.android.Client;

/**
 * Created by muhammadriyaz on 29/04/17.
 */

public class FloorDataApplication extends Application {
    private Client mKinveyClient;

    private static FloorDataApplication singleTon;

    public static FloorDataApplication getInstance() {
        return singleTon;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleTon = this;
    }

    public Client getKinveyClient() {
        if (mKinveyClient == null) {
            mKinveyClient = new Client.Builder("kid_Ska3IZG1-",
                    "ee9fde9bfc4e42568d00f9fd6412c984",
                    this.getApplicationContext()).build();
        }
        return mKinveyClient;
    }
}
