package com.riyazm.floordataapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kinvey.android.AsyncAppData;
import com.kinvey.android.Client;
import com.kinvey.android.callback.KinveyListCallback;
import com.kinvey.android.callback.KinveyMICCallback;
import com.kinvey.java.User;
import com.riyazm.floordataapp.FloorDataApplication;
import com.riyazm.floordataapp.R;
import com.riyazm.floordataapp.models.FloorData;
import com.riyazm.floordataapp.utilities.FloorView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();
    List<FloorData> list;
    RelativeLayout container;
    ProgressDialog dialog;
    ImageView bgImage;
    int startX = 0, startY = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        container = (RelativeLayout) findViewById(R.id.container);
        bgImage = (ImageView) findViewById(R.id.bg_image);

        bgImage.postDelayed(new Thread() {
            @Override
            public void run() {
                super.run();
                startX = bgImage.getLeft();
                startY = bgImage.getTop();

                Log.d(TAG, "startX: " + startX);
                Log.d(TAG, "startY: " + startY);

//                list.add(new FloorData("Drive", new Position(8, 8, 126, 88)));
//                list.add(new FloorData("CEO room", new Position(196, 10, 62, 46)));
//                list.add(new FloorData("CTO room", new Position(266, 10, 80, 46)));
//                list.add(new FloorData("Refrigeration", new Position(8, 104, 100, 70)));
//                list.add(new FloorData("HMP room", new Position(8, 180, 110, 60)));
//                list.add(new FloorData("Lighting zone", new Position(196, 64, 150, 108)));
//
//                plotData();
                getData();
            }
        }, 500);

        Log.d(TAG, "density: " + getDensityName(this));

        list = new ArrayList<>();
        dialog = new ProgressDialog(this);
        dialog.setTitle(getString(R.string.loading));

    }

    private static String getDensityName(Context context) {
        int densityDpi = context.getResources().getDisplayMetrics().densityDpi;
        String result = "ldpi";
        switch (densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                result = "ldpi";
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                result = "mdpi";
                break;
            case DisplayMetrics.DENSITY_TV:
            case DisplayMetrics.DENSITY_HIGH:
                result = "hdpi";
                break;
            case DisplayMetrics.DENSITY_XHIGH:
            case DisplayMetrics.DENSITY_280:
                result = "xhdpi";
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
            case DisplayMetrics.DENSITY_360:
            case DisplayMetrics.DENSITY_400:
            case DisplayMetrics.DENSITY_420:
                result = "xxhdpi";
                break;

            case DisplayMetrics.DENSITY_XXXHIGH:
            case DisplayMetrics.DENSITY_560:
                result = "xxxhdpi";
                break;
        }
        return result;
    }

    private void getData() {
        dialog.show();
        Client kinvey = FloorDataApplication.getInstance().getKinveyClient();
        if (kinvey.user().isUserLoggedIn()) {
            fetchFloorData(kinvey);
        } else {
            login(kinvey);
            fetchFloorData(kinvey);
        }

    }

    private void login(Client kinvey) {
        kinvey.user().login("testUser", "testPassword", new KinveyMICCallback() {
            @Override
            public void onReadyToRender(String s) {
            }

            @Override
            public void onSuccess(User user) {
                Log.d(TAG, "Login successful");
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d(TAG, "failed to login!");
                if (dialog != null && !isFinishing()) {
                    dialog.dismiss();
                }
                Toast.makeText(MainActivity.this, R.string.failed_to_login, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void plotData() {
        FloorView view = new FloorView(this, null, list, startX, startY);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        container.addView(view, params);
        Log.d(TAG, "container: " + container.getWidth() + " height: " + container.getHeight());
        if (dialog != null && !isFinishing()) {
            dialog.dismiss();
        }
    }

    private void fetchFloorData(Client kinvey) {
        AsyncAppData<FloorData> data = kinvey.appData("floorData", FloorData.class);
        data.get(new KinveyListCallback<FloorData>() {
            @Override
            public void onSuccess(FloorData[] floorDatas) {
                Log.d(TAG, "success reading collection!: " + floorDatas.length);
                for (FloorData data : floorDatas) {
                    Log.d(TAG, "data: " + data.toString());
                    list.add(data);
                }
                plotData();
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d(TAG, "failed reading data! " + throwable.toString());
                if (dialog != null && !isFinishing()) {
                    dialog.dismiss();
                }
                Toast.makeText(MainActivity.this, R.string.failed_to_fetch_data_please_retry, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
