package com.riyazm.floordataapp.models;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

/**
 * Created by muhammadriyaz on 29/04/17.
 */

public class FloorData extends GenericJson {
    @Key("name")
    String name;

    @Key("position")
    Position position;

//    public FloorData(String name, Position position){
//        this.name = name;
//        this.position =position;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "FloorData{" +
                "name='" + name + '\'' +
                ", position=" + position +
                '}';
    }
}
