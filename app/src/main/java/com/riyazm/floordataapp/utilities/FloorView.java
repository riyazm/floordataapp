package com.riyazm.floordataapp.utilities;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.riyazm.floordataapp.R;
import com.riyazm.floordataapp.models.FloorData;

import java.util.List;

/**
 * Created by muhammadriyaz on 29/04/17.
 */

public class FloorView extends View {
    public static final String TAG = FloorView.class.getSimpleName();
    Context context;
    List<FloorData> list;
    final int TEXT_SIZE = 10;
    final int TEXT_TOP_MARGIN = 10;
    final int TEXT_LEFT_MARGIN = 0;
    int START_X = 0;
    int START_Y = 0;

    public FloorView(Context context, @Nullable AttributeSet attrs, List<FloorData> list, int startX, int startY) {
        super(context, attrs);
        this.context = context;
        this.list = list;
        this.START_X = startX;
        this.START_Y = startY;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint paint = new Paint();
        for (FloorData data : list) {

            //background
            paint.setColor(ContextCompat.getColor(context, R.color.colorOverLay));
            paint.setStyle(Paint.Style.FILL);
            paint.setFlags(Paint.ANTI_ALIAS_FLAG);
            paint.setStrokeWidth(getDensityByPixel(1f));

            // rectangle
            float calculatedX = getDensityByPixel(data.getPosition().getX()) + START_X;
            float calculatedY = getDensityByPixel(data.getPosition().getY()) + START_Y;
            float calculatedWidth = getDensityByPixel(data.getPosition().getWidth()) + calculatedX;
            float calculatedHeight = getDensityByPixel(data.getPosition().getHeight()) + calculatedY;
            Log.d(TAG, " x: " + calculatedX);
            Log.d(TAG, " y: " + calculatedY);
            Log.d(TAG, " width: " + calculatedWidth);
            Log.d(TAG, " height: " + calculatedHeight);
            canvas.drawRect(
                    calculatedX,
                    calculatedY,
                    calculatedWidth,
                    calculatedHeight,
                    paint);

            // border
            paint.setColor(ContextCompat.getColor(context, R.color.colorRed));
            paint.setStyle(Paint.Style.STROKE);
            paint.setPathEffect(new DashPathEffect(new float[]{10, 20}, 0));
            canvas.drawRect(calculatedX, calculatedY,
                    calculatedWidth,
                    calculatedHeight, paint);

            //text
            paint.setStyle(Paint.Style.FILL);
            paint.setStrokeWidth(getDensityByPixel(0.5f));
            paint.setTextSize(getDensityByPixel(TEXT_SIZE));
            paint.setPathEffect(null);
            paint.setColor(ContextCompat.getColor(context, R.color.colorBlack));
            canvas.drawText(data.getName(),
                    calculatedX + getDensityByPixel(TEXT_LEFT_MARGIN),
                    calculatedY + getDensityByPixel(TEXT_TOP_MARGIN),
                    paint);
        }
    }


    float getDensityByPixel(float pixel) {
        float density = 0;
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        Log.d(TAG, "density: " + metrics.density);
        Log.d(TAG, "dpi: " + metrics.densityDpi);
        switch (metrics.densityDpi){
            case DisplayMetrics.DENSITY_LOW:
                // LDPI
                density = 0.75f;
                break;

            case DisplayMetrics.DENSITY_MEDIUM:
                // MDPI
                density = 1f;
                break;

            case DisplayMetrics.DENSITY_TV:
            case DisplayMetrics.DENSITY_HIGH:
                // HDPI
                density = 1.5f;
                break;

            case DisplayMetrics.DENSITY_XHIGH:
            case DisplayMetrics.DENSITY_280:
                // XHDPI
                density = 2f;
                break;

            case DisplayMetrics.DENSITY_XXHIGH:
            case DisplayMetrics.DENSITY_360:
            case DisplayMetrics.DENSITY_400:
            case DisplayMetrics.DENSITY_420:
                // XXHDPI
                density = 3.375f;
                break;

            case DisplayMetrics.DENSITY_XXXHIGH:
            case DisplayMetrics.DENSITY_560:
                // XXXHDPI
                density = 4f;
                break;

            default:
                density = metrics.density;
                break;
        }
        Log.d(TAG, "density1: " + density);
        return (int) (pixel * density);
    }
}
